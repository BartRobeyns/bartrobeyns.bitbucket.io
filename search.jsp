<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="oracle.ifs.beans.Folder" %>
<%@ page import="oracle.ifs.beans.SearchResultObject" %>
<%@ page import="oracle.ifs.common.IfsException" %>

<%@ page import="org.apache.regexp.RE" %>

<%@ page import="umicore.dams.base.DamsDocument" %>
<%@ page import="umicore.dams.base.RootFolder" %>
<%@ page import="umicore.dams.panes.function.search.SearchPane" %>
<%@ page import="umicore.dams.util.DamsUtil" %>
<%@ page import="umicore.dams.util.FileUtil" %>
<%@ page import="umicore.dams.util.IfsUtil" %>
<%@ page import="umicore.dams.categorization.CategoryTree"%>
<%@ page import="umicore.dams.categorization.CategoryTreeElement"%>

<% SearchPane searchPane = new SearchPane(request); %>
<% SimpleDateFormat sdf = new SimpleDateFormat( "dd-MMM-yy"); %>

<div id="<%= searchPane.getCssId() %>">
    <h1>Search</h1>

    <form method="post">
        <input type="hidden" name="action" value="Search"/>
    
        <table width="*" align="center" border="0" cellpadding="0" cellspacing="1" class="tableblank">
            <tr>
                <td align="right">
                    <i><b>Enter your query: </b></i>
                </td>
                <td>
                    <input type="text" name="<%= SearchPane.CRITERIUM_PARAMETER %>" size="60" maxlength="255" value="<%= searchPane.getCriteriumParameter() %>"/>
                </td>
                <td>
                    <input type="submit" value="Start"/>
                </td>
            </tr>
            <% CategoryTree[] trees = searchPane.getCategoryTrees();
            	if ( trees != null ) { %>
            <tr>
            	<td align="right">
            		<i><b>Category: </b></i>
            	</td>
            	<td colspan="2">
            			<%
            				for ( int i = 0; i < trees.length; i++ ) {
	            				CategoryTree tree = trees[i];
	            				%><select name="<%=SearchPane.CATEGORY_PARAMETER%>">
	   	         			<option value=""><%=searchPane.getMessage("search.select.category")%> <%=tree.getRootCategory().getName()%></option><%
	            				for ( Iterator iterator = tree.iterateElements(); iterator.hasNext(); ) {
	            					CategoryTreeElement treeElement = (CategoryTreeElement) iterator.next();
	            					%>
	            					<option value="<%=treeElement.getCategory().getId()%>"><%=treeElement.getCategory().getName()%></option>
	            				<% } %>
		            			</select>
		            		<% } %>
            	</td>
            </tr>
						<% } %>
            <tr>
                <td align="right">
                    <i><b>Site: </b></i>
                </td>
                <td colspan="2">
                    <select name="<%= SearchPane.FOLDER_PARAMETER %>">
                        <option value="<%= searchPane.getTypePath() %>"><%= searchPane.getMessage("search.anywhere") %></option>

                        <% Iterator subsites = searchPane.getSubsiteFolders().iterator(); %>

                        <% while (subsites.hasNext()) { %>
                            <% RootFolder subsite = (RootFolder) subsites.next(); %>

                            <% if (subsite.getDescription() != null) { %>
                                <%
                                String subsiteDescription = DamsUtil.escapeString(subsite.getDescription());
                                String subsitePath = subsite.getAnyFolderPath();
                                %>

                                <% if (subsiteDescription != null && subsiteDescription.length() != 0) { %>
                                    <% if (searchPane.getFolderParameter().equals(subsitePath)) { %>
                                        <option value="<%= subsitePath %>" selected><%= subsiteDescription %></option>
                                    <% } else { %>
                                        <option value="<%= subsitePath %>"><%= subsiteDescription %></option>
                                    <% } %>
                                <% } %>
                            <% } %>
                        <% } %>
                    </select>
                </td>
            </tr>
        </table>
    </form>

    <p>
        <hr width="80%" align="center" size="3"/>
    </p>

    <% if (searchPane.isCriteriumPresent()) { %>
        <% try { %>
	    	<% SearchResultObject[] results = searchPane.doSearch(); %>

    	    <% if (results != null) { %>
			    <% if (results.length != 0) { %>
                    <table align="center" border="0" cellpadding="3" cellspacing="3">
                        <tr>
                            <td colspan="6"><%= results.length %> documents found for query: "<%= searchPane.getCriteriumParameter() %>"</td>
                        </tr>
                        <tr class="header">
                            <th align="center">&nbsp;Relevancy&nbsp;</th>
                            <th align="center">#</th>
                            <th align="center">&nbsp;Description <em>(Name)</em>&nbsp;</th>
                            <th colspan="2" align="center">Size - Date</th>
                            <th align="center">Site</th>
                        <tr>

                        <% int docPublished = 0; %>

        				<% for (int i = 0; i < results.length; i++) { %>
                            <%
                            SearchResultObject result = results[i];
                            DamsDocument doc = (DamsDocument) result.getLibraryObject();
                            String folderPath = null;

                            if (doc.getFamily() != null) {
                                folderPath = doc.getFamily().getAnyFolderPath();
                            }
                            else {
                                folderPath = doc.getAnyFolderPath();
                            }
                            %>
                    
                        	<% if (folderPath != null) { %>
                                <%
                                Folder subsite = (Folder) searchPane.getIfsFileSystem().findPublicObjectByPath(IfsUtil.getSubsite(folderPath));

                                String subsitePath = subsite.getName();
                                String subsiteDesc = subsite.getDescription();
                                String topicPath = "/";
                                String docName = doc.getName();
                                String docDesc = doc.getDescription();

                                if (docDesc == null) {
                                    docDesc = docName;
                                }

                                if (IfsUtil.getTopicpath(folderPath) != null) {
                                    topicPath = IfsUtil.getTopicpath(folderPath) + "/";
                                }
                                %>

                                <% if (doc.getPublished() != null && doc.getPublished().booleanValue()) { %>
                                    <tr class="alternateRow">
                                        <%
                                        int score = result.getScore("CQ");

                                        docPublished++;
                                        %>

                                        <td>
                                            <table width="*" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="*">
                                                        <table width="*" border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="tablegraph" height="12" width="<%= score * 0.8 %>"/>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="*">
                                                        <i>&nbsp;<%= score %>&nbsp;%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right">
                                            <b>&nbsp;<%= docPublished %>.&nbsp;</b>
                                        </td>
                                        <td>
                                            <a href="<%= searchPane.getDamsRequest().urlToPO(doc) %>"><%= docDesc %></a> <em>(<%= docName %>)</em>
                                        </td>
                                        <td><%= doc.getContentSize() %> bytes</td>
                                        <td> - <%= sdf.format(doc.getLastModifyDate()) %></td>
                                        <td>&nbsp;<%= subsiteDesc %>&nbsp;</td>
                                    </tr>
                                <% } %>
                            <% } %>
	           			<% } %>
                    </table>
			    <% } else { %>
                    No results were found for your query <i><%= searchPane.getCriteriumParameter() %></i>.
    			<% } %>
	    	<% } else { %>
                No results were found for your query <i><%= searchPane.getCriteriumParameter() %></i>.
            <% } %>
        <% } catch (IfsException ie) { %>
            Your query <i><%= searchPane.getCriteriumParameter() %></i> contains some invalid operators. Please refer to the <a href="javascript:openUrl('group', 'help/', 'query.html');">Query help file</a> for more information.
        <% } %>
    <% } %>
</div>