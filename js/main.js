function Question(operator,operand1,operand2) {
	this.operator = operator;
	this.operand1 = operand1;
	this.operand2 = operand2;
	console.log('Question(' + operator + ',' + operand1 + ',' + operand2 + ')'  );
    if ( this.operator == '/') {
    	this.origoperand1 = this.operand1;
      	this.operand1 = this.testCalculate();
    }
};

Question.prototype.toString = function() {
    return this.operand1 + ' ' + this.operator + ' ' + this.operand2; 
};

Question.prototype.toBeautyString = function() {
    return this.operand1 + ' ' + beautyOperators[this.operator] + ' ' + this.operand2; 
};

Question.prototype.isLegal = function() {
	var result = eval(this.toString());
	return !isNaN(result);
}
Question.prototype.calculate = function() {
	return eval(this.toString());
}
Question.prototype.testCalculate = function() {
	 if ( this.operator == '/') {
    	return this.origoperand1 * this.operand2; 
    }
	return eval(this.toString());
}
Question.prototype.invertOperator = function() {
	return (this.operator=='+'?'-':(this.operator=='-'?'+':(this.operator=='*'?'/':(this.operator=='/'?'*':'?'))));
}


	var types = {
		random: function() {return buildQuestion()},
		table2: function() {return buildQuestion('*', undefined, undefined, 2,2)},
		table3: function() {return buildQuestion('*', undefined, undefined, 3,3)},
		table4: function() {return buildQuestion('*', undefined, undefined, 4,4)},
		table5: function() {return buildQuestion('*', undefined, undefined, 5,5)},
		table6: function() {return buildQuestion('*', undefined, undefined, 6,6)},
		table7: function() {return buildQuestion('*', undefined, undefined, 7,7)},
		table8: function() {return buildQuestion('*', undefined, undefined, 8,8)},
		table9: function() {return buildQuestion('*', undefined, undefined, 9,9)},
		table10: function() {return buildQuestion('*', undefined, undefined, 10,10)},
		multiply: function() {return buildQuestion('*', undefined, undefined, undefined, undefined)},
		sum: function() {return buildQuestion('+', undefined, undefined, undefined, undefined)},
		subract: function() {return buildQuestion('-', undefined, undefined, undefined, undefined)},
		divide: function() {return buildQuestion('/', undefined, undefined, 1,10);},
		divide2: function() {return buildQuestion('/', undefined, undefined, 2,2);},
		divide3: function() {return buildQuestion('/', undefined, undefined, 3,3);},
		divide4: function() {return buildQuestion('/', undefined, undefined, 4,4);},
		divide5: function() {return buildQuestion('/', undefined, undefined, 5,5);},
		divide6: function() {return buildQuestion('/', undefined, undefined, 6,6);},
		divide7: function() {return buildQuestion('/', undefined, undefined, 7,7);},
		divide8: function() {return buildQuestion('/', undefined, undefined, 8,8);},
		divide9: function() {return buildQuestion('/', undefined, undefined, 9,9);},
		divide10: function() {return buildQuestion('/', undefined, undefined, 10,10);}
	};
	var beautyOperators = {
		'/':':',
		'*':'x',
		'+':'+',
		'-':'-'
		}
	var result;
	var exercises = ['random'];
	var operators = ['+','*','/','-'];
	var defaultLower = 0;
	var defaultUpper = 10;
	var maxResult = 100;
	var minResult = 0;
	var nrOfQuestions=20;
	var sessionCount = 0;
	var startTime=0;
	var question;
	var isClockLimit = false;
	var clockLimit = 0;
	var clockStartTime = 0;
	var isCountLimit = true;
	var counter = 0;
	var translator = new Translator();
	function rangeRandom(lower,upper) {
		if ( typeof lower == 'undefined') {
			lower = defaultLower;
			upper = defaultUpper;
		}
		return Math.ceil(Math.random()*(upper-lower + 1)) + lower -1;

	}
	function buildQuestion(operator, operand1Lower, operand1Upper, operand2Lower, operand2Upper) {
		if ( typeof operator == 'undefined' ) {
			operator = operators[Math.ceil(Math.random()*operators.length )-1];
		}
		
		return new Question(operator, rangeRandom(operand1Lower, operand1Upper), rangeRandom(operand2Lower, operand2Upper)); 
		
	}
	function clockRanOut() {
		console.log(new Date().getTime() - clockStartTime);
		console.log(clockLimit * 60);
		return isClockLimit && ((new Date().getTime() - clockStartTime)/1000 > clockLimit * 60);
}
	function setQuestion() {
		if ( clockRanOut() || (isCountLimit && (++sessionCount > nrOfQuestions) )) {
			var seconds = Math.round((new Date().getTime() - clockStartTime)/1000);
			$('#question').html('<div id="endreport">' + translator.getTranslation('app.finished')
				+ '<br>' + (counter) + ' ' + translator.getTranslation('settings.exercises')
				+ ' ' + translator.getTranslation('global.in') + ' ' + seconds + ' sec.</div>');
			return;
		}
		var exerciseIndex = Math.ceil(Math.random()*exercises.length)-1;
		var testedQuestion, testResult;
		do {
			testedQuestion = types[exercises[exerciseIndex]]();
			testResult = testedQuestion.testCalculate();
			
		} while ( !testedQuestion.isLegal() || (minResult > testResult) || (testResult > maxResult));
		question = testedQuestion.toBeautyString();
		result = testedQuestion.calculate();
		
		$('#question').html(question);
		startTime = new Date().getTime();
	}
	function checkAnswer() {
		
	}
	function startQaLoop() {
		initialize();
		$('div#report').empty();
		sessionCount = 0;
		setQuestion();
	}
	
	function initialize() {
		exercises = [];
		$('input[name="exercise"]:checked').each(function() {exercises[exercises.length] = $(this).val();});
		defaultLower   = parseInt($('input[name="lowerbound"]').val());
		defaultUpper   = parseInt($('input[name="upperbound"]').val());
		minResult      = parseInt($('input[name="minresult"]').val());
		maxResult      = parseInt($('input[name="maxresult"]').val());
		nrOfQuestions  = parseInt($('input[name="nrofquestions"]').val());
		clockLimit = parseInt($('input[name="timelimit"]').val());
		isClockLimit = ($('input[name="limit"]:checked').val() == 'clock');
		isCountLimit = !isClockLimit;
		clockStartTime = new Date().getTime();
		counter = 0;
	}

	function switchLanguage(lang) {
		translator.setLanguage(lang);
	}
	
	$('document').ready(function($){
		translator.setLanguage('nl');
		initialize();
		//$('input#answer').keypad({keypadClass: 'midnightKeypad',keypadOnly:false, prompt: '', closeText: 'X', clearText: '«', backText: '‹'});
		$('button#start').click(function(){startQaLoop();$('input#answer').focus();});
		$('#settingslabel').click(function(){$('#settings').toggle()} );
		$('input[name="exercise"],input[name="lowerbound"],input[name="upperbound"],input[name="minresult"],input[name="maxresult"],input[name="nrofquestions"]').change(function(){initialize();});
		$('input#answer').bind("change blur keyup mouseup", function() {
			
			if ($(this).val() == result) {
				var end = new Date().getTime();
				$('<div>' + ++counter + ". " + question + ' = ' + result + ' (' + parseFloat(Math.round((end-startTime)/10)/ 100).toFixed(2)  + ')</div>').prependTo($('div#report'));
				$(this).val('');
				setQuestion();
			}});
	})
